#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

//affichage de la demande de la mise pour la partie de roulette
void miseRoulette(int jeton)
{
    cout << "Vous avez " << jeton << " jetons." << endl << "Combien voulez-vous parier ? ";
}
//affichage de la demande du pari pour la partie de roulette
void choixRoulette(int mise)
{
    cout << endl << "Votre mise est de "<< mise <<". " << endl << "Jouez ! Pair [0] ou Impair [1] : ";
}
//affichage du choix lors de la roulette russe, rejouer ou passer � la roulette
void choixRouletteRusse(void)
{
    cout << endl << "Voulez-vous tirer une nouvelle fois ? [0]" << endl << "Ou Voulez-vous repasser a la roulette ? [1] ";
}

int JouerRoulette(int jeton)
{
    int mise=0;
    int choix=0;
    int roulette;

    //le joueur joue � la roulette
    miseRoulette(jeton);
    cin >> mise;
    //controle sur la saisie de la mise
    while (mise < 1 || mise > 25 || mise > jeton)
    {
        miseRoulette(jeton);
        cin >> mise;
    }
    //le joueur parie sur pair ou impair (pair:0; impair:1)
    choixRoulette(mise);
    cin >> choix;
    //controle sur la saisie du pari
    while (choix < 0 || choix > 1)
    {
        choixRoulette(mise);
        cin >> choix;
    }
    //La bille est lanc�e
    roulette=rand()%36+1;
    cout << endl << "Le " << roulette << " sort. ";
    if (choix == roulette%2)
    {
        cout << "Gagne !" << endl;
        jeton+=mise*2;
    }
    else
    {
        cout << "Perdu !" << endl;
        jeton-=mise;
    }
    cout << endl;
    return jeton;
}

int jouerRouletteRusse()
{
    int jeton=0;
    int choix=0;
    int position;
    bool continuer=true;
    bool vivant=true;
    int barillet[6];

    //Le joueur joue � la roulette russe
    cout << "Vous n'avez plus de jetons ! Prenez le revolver !" << endl << endl;
    //r�initialisation du revolver et variables du jeu
    continuer=true;
    for (int i=0; i<6;++i)
    {
        barillet[i]=0;
    }
    //positionement de la cartouche dans le barillet
    position=rand()%6;
    barillet[position]=1;
    system("pause"); //dramatisation du jeu!
    //le jeu de la roulette russe continue tant que le joueur le veut et s'il est vivant
    while (continuer && vivant)
    {
        //on tire une premi�re fois
        if (barillet[0]==1) //Fin de partie, le joueur est mort
        {
            cout << endl << "BANG ! "<< endl <<endl;
            vivant=false;
            return 0;//si le joueur meurt, on renvoit 0
        }
        else //sinon on vous propose de rejouer.
        {
            cout << endl << "Clic !" << endl << "Vous avez eu de la chance !";
            choixRouletteRusse();
            cin >> choix;
            //controle sur la saisie du choix
            while (choix < 0 || choix > 1)
            {
                choixRouletteRusse();
                cin >> choix;
            }
            //Si la balle n�est pas dans la chambre lorsque le joueur presse la d�tente, il gagne 20 jetons
            jeton+=20;
            //d�placement de la cartouche dans le barillet
            position--;
            barillet[position]=1;
            //est-ce que le joueur veut continuer � la roulette russe?
            if (choix==1)
            {
                continuer=false;
                cout << endl << endl;
            }
        }
    }
    return jeton;
}

int main()
{
    srand(time(NULL));
    int jeton=10;
    bool vivant=true;

    cout << "Priviet! Kak diela ?" << endl << endl; //Salut! Comment �a va ?
    //le jeu continue tant que le joueur a moins de 100 jetons et qu'il est en vie
    while (jeton<100 && vivant) //vivant==true
    {
        //le joueur joue � la roulette
        jeton=JouerRoulette(jeton);
        //si le joueur n'a plus de jetons, il joue � la roulette russe
        if (jeton == 0)
        {
            jeton=jouerRouletteRusse();
            if (jeton==0) //si la fonction de jeu de la roulette russe renvoit 0, le joueur est mort
            {
                vivant=false;
            }
        }
    }
    if (vivant) //fin de partie, le joueur est vivant
    {
            cout << "Rentrez chez vous et ne vous retournez pas !" <<endl;
    }
    return 0;
}
